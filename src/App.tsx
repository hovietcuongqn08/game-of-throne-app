import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Login from './pages/login'
import Container from '@material-ui/core/Container';
import Main from './pages/main'

export default () => {
  return(
    <Container component="main">
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Main/>
          </Route>
        </Switch>
      </Router>
    </Container>
  )
}