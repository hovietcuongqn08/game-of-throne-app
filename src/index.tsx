import "regenerator-runtime/runtime";
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import App from './App';
import * as serviceWorker from './serviceWorker';
import createSagaMiddleware from "redux-saga";
import rootReducer from "./redux/reducers/rootReducers";
import mySaga from "./redux/saga/saga";
import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import { Auth0Provider } from "@auth0/auth0-react";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(mySaga);

ReactDOM.render(    
  <Auth0Provider
    domain="dev-8zqu96b7.us.auth0.com"
    clientId="wPoPH3r1DjrDBpjtKLc0Crs2pISSN4DZ"
    redirectUri={window.location.origin}
  >
    <Provider store={store}>
      <App />
    </Provider>
  </Auth0Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
