import React from 'react'
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';

export type HouseProps = {
  url: string,
  name: string,
  region: string,
  coatOfArms: string,
  words: string,
  titles: Array<string>,
  seats: Array<string>
  handleToggle: Function
}

const useStyles = makeStyles((theme) => ({
  house: {
    borderRadius: 5,
    border: "1px solid rgba(0, 0, 0, 0.23)",
    padding: theme.spacing(1),
    height: "100%",
    boxSizing: "border-box",
    cursor: "pointer",

    "&:hover": {
      opacity: 0.5
    }
  },
  houseContent: {
    marginTop: theme.spacing(1)
  },
  houseName: {

  },
  houseField: {
    fontSize: 18,

    "&:not(:first-child)": {
      marginTop: theme.spacing(1)
    }
  },
  houseLabel: {
    fontWeight: "bold"
  }
}));

export default (props: HouseProps) => {
  const { url, name, region, coatOfArms, handleToggle } = props
  const dispatch = useDispatch()
  const openDetail = () => {
    dispatch({ type: "FETCH_HOUSE_DETAIL", payload: url.substring(url.lastIndexOf('/') + 1) })
    handleToggle(true)
  }
  const classes = useStyles()
  return (
    <Box p={1.5} className={classes.house} onClick={() => openDetail()}>
      <div className={classes.houseContent}>
        <Typography component="h4" variant="h5" className={classes.houseName}>{name || "Unknow House"}</Typography>
        <div className={classes.houseField}>
          <span className={classes.houseLabel}>Region: </span>
          {region}
        </div>
        <div className={classes.houseField}>
          <span className={classes.houseLabel}>Coat of arms: </span>
          {coatOfArms || "?"}
        </div>
      </div>
    </Box>
  )
}