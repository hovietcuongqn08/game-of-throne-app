import React from 'react'
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';

export type CharacterProps = {
  url: string,
  name: string,
  gender: string,
  culture: string,
  born: string,
  titles: Array<string>,
  aliases: Array<string>,
  handleToggle: Function
}

const useStyles = makeStyles((theme) => ({
  character: {
    borderRadius: 5,
    border: "1px solid rgba(0, 0, 0, 0.23)",
    padding: theme.spacing(1),
    height: "100%",
    boxSizing: "border-box",
    cursor: "pointer",

    "&:hover": {
      opacity: 0.5
    }
  },
  characterContent: {
    marginTop: theme.spacing(1)
  },
  characterName: {

  },
  characterField: {
    fontSize: 18,

    "&:not(:first-child)": {
      marginTop: theme.spacing(1)
    }
  },
  characterLabel: {
    fontWeight: "bold"
  }
}));

export default (props: CharacterProps) => {
  const { url, name, gender, culture, handleToggle } = props
  const dispatch = useDispatch()
  const openDetail = () => {
    dispatch({ type: "FETCH_CHARACTER_DETAIL", payload: url.substring(url.lastIndexOf('/') + 1) })
    handleToggle(true)
  }

  const classes = useStyles()

  return (
    <Box p={1.5} className={classes.character} onClick={() => openDetail()}>
      <div className={classes.characterContent}>
        <Typography component="h4" variant="h5" className={classes.characterName}>{name || "Unknow Character"}</Typography>
        <div className={classes.characterField}>
          <span className={classes.characterLabel}>Gender: </span>
          {gender}
        </div>
        <div className={classes.characterField}>
          <span className={classes.characterLabel}>Culture: </span>
          {culture || "?"}
        </div>
      </div>
    </Box>
  )
}