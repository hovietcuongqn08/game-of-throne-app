import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
import { RootReducer } from "./../../../redux/reducers/rootReducers"
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles  } from '@material-ui/core/styles';

export type DetailModalProps = {
  isModalOpen: boolean,
  handleToggle: Function,
  detailData?: any
}

const useStyles = makeStyles((theme) => ({
  modal: {
    borderRadius: 5,
    border: "1px solid rgba(0, 0, 0, 0.23)",
    padding: theme.spacing(1),
    height: "100%",
    boxSizing: "border-box",
  },
  modalContent: {
    marginTop: theme.spacing(1)
  },
  modalName: {

  },
  modalField: {
    fontSize: 18,

    "&:not(:first-child)": {
      marginTop: theme.spacing(1)
    }
  },
  modalLabel: {
    fontWeight: "bold"
  }
}));

export default (props: DetailModalProps) => {
  const {isModalOpen, handleToggle} = props
  const detailSelector = useSelector((state: RootReducer) => state.main.detail)
  const dispatch = useDispatch()
  const classes = useStyles()

  const { 
    name,
    gender,
    culture,
    born,
    titles,
    aliases,
    region,
    coatOfArms,
    words,
    seats,
    country,
    authors,
    publisher,
    released
  } = detailSelector

  const onClose = () => {
    dispatch({ type: "CLEAR_DETAIL" })
    handleToggle(false)
  }

  return (
    <div>
      <Dialog
        open={isModalOpen}
        onClose={() => onClose()}
        aria-labelledby="responsive-dialog-title"
        className={classes.modal}
      >
        <DialogTitle id="responsive-dialog-title">Detail Modal</DialogTitle>
        <DialogContent>
          {gender && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Name: </span>
            {name}
          </div>}
          {gender && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Gender: </span>
            {gender}
          </div>}
          {culture && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Culture: </span>
            {culture || "?"}
          </div>}
          {born && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Born: </span>
            {born || "?"}
          </div>}
          {titles && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Titles: </span>
            {titles.toString() || "?"}
          </div>}
          {aliases && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Aliases: </span>
            {aliases.toString() || "?"}
          </div>}
          {region && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Region: </span>
            {region}
          </div>}
          {coatOfArms && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Coat of arms: </span>
            {coatOfArms || "?"}
          </div>}
          {words && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Words: </span>
            {words || "?"}
          </div>}
          {seats && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Seats: </span>
            {seats.toString() || "?"}
          </div>}
          {country && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Country: </span>
            {country}
          </div>}
          {released && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Released: </span>
            {moment(released).format("DD/MM/yyyy")}
          </div>}
          {authors && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Authors: </span>
            {authors.toString()}
          </div>}
          {publisher && <div className={classes.modalField}>
            <span className={classes.modalLabel}>Publisher: </span>
            {publisher}
          </div>}
        </DialogContent>
        <DialogActions>
          <Button onClick={() => onClose()} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
