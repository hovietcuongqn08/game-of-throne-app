import React from 'react'
import moment from 'moment'
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';

export type BookProps = {
  url: string,
  name: string,
  country: string,
  released: Date,
  authors: Array<string>,
  publisher: string,
  handleToggle: Function
}

const useStyles = makeStyles((theme) => ({
  book: {
    borderRadius: 5,
    border: "1px solid rgba(0, 0, 0, 0.23)",
    padding: theme.spacing(1),
    height: "100%",
    boxSizing: "border-box",
    cursor: "pointer",

    "&:hover": {
      opacity: 0.5
    }
  },
  bookContent: {
    marginTop: theme.spacing(1)
  },
  bookName: {

  },
  bookField: {
    fontSize: 18,

    "&:not(:first-child)": {
      marginTop: theme.spacing(1)
    }
  },
  bookLabel: {
    fontWeight: "bold"
  }
}));

export default (props: BookProps) => {
  const { url, name, country, released, handleToggle } = props
  const dispatch = useDispatch()
  const classes = useStyles()

  const openDetail = () => {
    dispatch({ type: "FETCH_BOOK_DETAIL", payload: url.substring(url.lastIndexOf('/') + 1) })
    handleToggle(true)
  }

  return (
    <Box p={1.5} className={classes.book} onClick={() => openDetail()}>
      <div className={classes.bookContent}>
        <Typography component="h4" variant="h5" className={classes.bookName}>{name}</Typography>
        <div className={classes.bookField}>
          <span className={classes.bookLabel}>Country: </span>
          {country}
        </div>
        <div className={classes.bookField}>
          <span className={classes.bookLabel}>Released: </span>
          {moment(released).format("DD/MM/yyyy")}
        </div>
      </div>
    </Box>
  )
}