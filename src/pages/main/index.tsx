import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { RootReducer } from "./../../redux/reducers/rootReducers"
import MainBook from "./main-book"
import MainCharacter from "./main-character"
import MainHouse from "./main-house"
import DetailModal from "./main-modal"
import Button from '@material-ui/core/Button'
import { useAuth0 } from "@auth0/auth0-react";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
  },
  paperHead: {
    display: "flex",
    justifyContent: "space-between"
  },
  searchBar: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing(2)
  },
  searchInput: {
    flex: 1,
  },
  searchResult: {
    marginTop: theme.spacing(3)
  }
}));


export default () => {
  const classes = useStyles()
  const { logout } = useAuth0();
  const mainSelector = useSelector((state: RootReducer) => state.main)
  const [keyword, handleChangeKeyword] = useState("")
  const [isModalOpen, handleToggle] = useState(false)
  const { books, characters, houses } = mainSelector
  const dispatch = useDispatch()
  
  useEffect(() => {
    dispatch({ type: "FETCH_BOOKS", payload: keyword })
    dispatch({ type: "FETCH_CHARACTERS", payload: keyword })
    dispatch({ type: "FETCH_HOUSES", payload: keyword })
  }, [dispatch, keyword])

  return (
    <Container>
      <DetailModal isModalOpen={isModalOpen} handleToggle={handleToggle}/>
      <div className={classes.paper}>
        <div className={classes.paperHead}>
          <Typography component="h1" variant="h5">
            Welcome to Game of Throne Fan App
          </Typography>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={() => logout()}
          >
            Logout
          </Button>
        </div>
        <div className={classes.searchBar}>
          <TextField
            className={classes.searchInput}
            variant="outlined"
            margin="normal"
            id="search"
            placeholder="Type character, book or house name to search"
            name="email"
            autoFocus
            value={keyword}
            onChange={e => handleChangeKeyword(e.target.value)}
          />
        </div>
        <div className={classes.searchResult}>
          <Grid container spacing={2}>
            {books.map((book, index) => 
              <Grid item xs={4} md={3} key={index}>
                <MainBook {...book} handleToggle={handleToggle} />
              </Grid>
            )}
            {characters.map((character, index) => 
              <Grid item xs={4} md={3} key={index}>
                <MainCharacter {...character} handleToggle={handleToggle} />
              </Grid>
            )}
            {houses.map((house, index) => 
              <Grid item xs={4} md={3} key={index}>
                <MainHouse {...house} handleToggle={handleToggle} />
              </Grid>
            )} 
          </Grid>
        </div>
      </div>
    </Container>
  )
}