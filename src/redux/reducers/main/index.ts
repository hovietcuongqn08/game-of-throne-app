import { BookProps } from "./../../../pages/main/main-book"
import { CharacterProps } from "./../../../pages/main/main-character"
import { HouseProps } from "./../../../pages/main/main-house"

const INITIAL_STATE = {
  books: [],
  characters: [],
  houses: [],
  detail: {}
}

export type Action = {
  type: string,
  payload: {
    data: any
  }
}

export type MainState = {
  books: Array<BookProps>
  characters: Array<CharacterProps>
  houses: Array<HouseProps>,
  detail: any
}

const main = (state = INITIAL_STATE, action: Action) => {
  switch (action.type) {
    case "FETCH_BOOKS_SUCCESS":
      return {
        ...state,
        books: action.payload.data
      }
    case "FETCH_CHARACTERS_SUCCESS":
      return {
        ...state,
        characters: action.payload.data
      }
    case "FETCH_HOUSES_SUCCESS":
      return {
        ...state,
        houses: action.payload.data
      }
    case "FETCH_DETAIL_SUCCESS":
      return {
        ...state,
        detail: action.payload.data
      }
    case "CLEAR_DETAIL":
      return {
        ...state,
        detail: {}
      }
    default:
      return state
  }
}

export default main
