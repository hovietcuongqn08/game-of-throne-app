import { combineReducers } from "redux"
import main, { MainState } from "./main"

export type RootReducer = {
  main: MainState
}

const rootReducers = combineReducers({
  main
})
export default rootReducers
