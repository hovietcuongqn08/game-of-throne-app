import { call, put } from "redux-saga/effects"
import axios from "axios"
import {
  fetchBooksSuccess,
  fetchBooksFail,
  fetchCharactersSuccess,
  fetchCharactersFail,
  fetchHousesSuccess,
  fetchHousesFail,
  fetchDetailSuccess,
  fetchDetailFail
} from "../../actions/main"
import { Action } from "./../../reducers/main"

const fetchBooksFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/books?name=${payload}&pageSize=50`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const fetchCharactersFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/characters?name=${payload}&pageSize=50`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const fetchHousesFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/houses?name=${payload}&pageSize=50`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})


const fetchBookDetailFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/books/${payload}`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const fetchCharacterDetailFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/characters/${payload}`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

const fetchHouseDetailFromAPI = (payload: any) => axios({
  method: "GET",
  url: `https://anapioficeandfire.com/api/houses/${payload}`,
  withCredentials: false,
  headers: {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  }
})

export function* fetchBooksSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchBooksFromAPI(payload))
    console.log(response)
    yield put(fetchBooksSuccess(response))
  } catch (error) {
    yield put(fetchBooksFail(error))
  }
}

export function* fetchCharactersSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchCharactersFromAPI(payload))
    yield put(fetchCharactersSuccess(response))
  } catch (error) {
    yield put(fetchCharactersFail(error))
  }
}

export function* fetchHousesSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchHousesFromAPI(payload))
    yield put(fetchHousesSuccess(response))
  } catch (error) {
    yield put(fetchHousesFail(error))
  }
}

export function* fetchBookDetailSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchBookDetailFromAPI(payload))
    yield put(fetchDetailSuccess(response))
  } catch (error) {
    yield put(fetchDetailFail(error))
  }
}

export function* fetchCharacterDetailSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchCharacterDetailFromAPI(payload))
    yield put(fetchDetailSuccess(response))
  } catch (error) {
    yield put(fetchDetailFail(error))
  }
}

export function* fetchHouseDetailSaga(action: Action) {
  const { payload } = action
  try {
    const response = yield call(() => fetchHouseDetailFromAPI(payload))
    yield put(fetchDetailSuccess(response))
  } catch (error) {
    yield put(fetchDetailFail(error))
  }
}
