import { takeEvery } from "redux-saga/effects"
import { 
  fetchBooksSaga, 
  fetchCharactersSaga, 
  fetchHousesSaga, 
  fetchBookDetailSaga, 
  fetchCharacterDetailSaga, 
  fetchHouseDetailSaga 
} from "./main"

function* mySaga() {
  yield takeEvery("FETCH_BOOKS", fetchBooksSaga)
  yield takeEvery("FETCH_CHARACTERS", fetchCharactersSaga)
  yield takeEvery("FETCH_HOUSES", fetchHousesSaga)
  yield takeEvery("FETCH_BOOK_DETAIL", fetchBookDetailSaga)
  yield takeEvery("FETCH_CHARACTER_DETAIL", fetchCharacterDetailSaga)
  yield takeEvery("FETCH_HOUSE_DETAIL", fetchHouseDetailSaga)
}

export default mySaga
