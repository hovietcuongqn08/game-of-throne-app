export const fetchBooks = (keyword: string) => ({
  type: "FETCH_BOOKS",
  payload: keyword
})

export const fetchBooksSuccess = (response: any) => ({
  type: "FETCH_BOOKS_SUCCESS",
  payload: response
})

export const fetchBooksFail = (response: any) => ({
  type: "FETCH_BOOKS_FAIL",
  payload: response
})

export const fetchCharacters = (keyword: string) => ({
  type: "FETCH_CHARACTERS",
  payload: keyword
})

export const fetchCharactersSuccess = (response: any) => ({
  type: "FETCH_CHARACTERS_SUCCESS",
  payload: response
})

export const fetchCharactersFail = (response: any) => ({
  type: "FETCH_CHARACTERS_FAIL",
  payload: response
})

export const fetchHouses = (keyword: string) => ({
  type: "FETCH_HOUSES",
  payload: keyword
})

export const fetchHousesSuccess = (response: any) => ({
  type: "FETCH_HOUSES_SUCCESS",
  payload: response
})

export const fetchHousesFail = (response: any) => ({
  type: "FETCH_HOUSES_FAIL",
  payload: response
})

export const fetchBookDetail = (id: string) => ({
  type: "FETCH_BOOK_DETAIL",
  payload: id
})

export const fetchCharacterDetail = (keyword: string) => ({
  type: "FETCH_CHARACTER_DETAIL",
  payload: keyword
})

export const fetchHouseDetail = (keyword: string) => ({
  type: "FETCH_HOUSE_DETAIL",
  payload: keyword
})

export const fetchDetailSuccess = (response: any) => ({
  type: "FETCH_DETAIL_SUCCESS",
  payload: response
})

export const fetchDetailFail = (response: any) => ({
  type: "FETCH_DETAIL_FAIL",
  payload: response
})

export const clearDetail = () => ({
  type: "CLEAR_DETAIL"
})